-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 12:24 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `internet_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `Cart_Id` int(11) NOT NULL,
  `Session_id` varchar(40) NOT NULL,
  `Product_id` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`Cart_Id`, `Session_id`, `Product_id`, `Quantity`) VALUES
(35, 'Hcc2STj1tcmqQCosP52QM5nOwz05q21516505669', 4, 4),
(38, 'iZ4d3Q4qlX7GxMibthcGuXVnqJ3ZE71516572092', 5, 2),
(39, 'iZ4d3Q4qlX7GxMibthcGuXVnqJ3ZE71516572092', 4, 1),
(40, '9EIG1dndwHsw7bbsMgglY2V1E5SRix1516576057', 4, 3),
(41, 'iGOCICGT92ZFjhcZrdhteaIeUYrPb01516576881', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `Category_Id` int(11) NOT NULL,
  `Category_Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`Category_Id`, `Category_Name`) VALUES
(1, 'Sosy'),
(2, 'Napoje');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `customer` varchar(50) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `customer`, `address`, `email`) VALUES
(1, '', '', ''),
(2, 'wer', 'aaa', 'wer@rew'),
(3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `orderproduct`
--

CREATE TABLE `orderproduct` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderproduct`
--

INSERT INTO `orderproduct` (`id`, `order_id`, `product_id`, `quantity`) VALUES
(1, 2, 5, 2),
(2, 2, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `Product_Id` int(255) NOT NULL,
  `Product_Index` varchar(10) NOT NULL,
  `Product_Name` char(255) NOT NULL,
  `Product_Price` float NOT NULL,
  `Product_Category_Id` int(11) NOT NULL,
  `Product_Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`Product_Id`, `Product_Index`, `Product_Name`, `Product_Price`, `Product_Category_Id`, `Product_Description`) VALUES
(4, 'sos01', 'Majonez kielecki 310ml', 3.99, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus leo turpis, vel vulputate elit dignissim ac. Sed nulla mi, accumsan ut pharetra id, aliquet ac orci. Sed egestas lacus vitae enim lobortis sollicitudin. Vivamus in laoreet elit, vitae finibus ex. Nullam auctor mi nisi, in pharetra urna pharetra at. Phasellus nec arcu mauris. Etiam commodo blandit cursus. '),
(5, 'nap01', 'Cisowianka niegazowana 1,5 l', 1.39, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus leo turpis, vel vulputate elit dignissim ac. Sed nulla mi, accumsan ut pharetra id, aliquet ac orci. Sed egestas lacus vitae enim lobortis sollicitudin. Vivamus in laoreet elit, vitae finibus ex. Nullam auctor mi nisi, in pharetra urna pharetra at. Phasellus nec arcu mauris. Etiam commodo blandit cursus. '),
(6, 'sos02', 'Lowicz Sos slodko-kwasny 350 g', 3.99, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus leo turpis, vel vulputate elit dignissim ac. Sed nulla mi, accumsan ut pharetra id, aliquet ac orci. Sed egestas lacus vitae enim lobortis sollicitudin. Vivamus in laoreet elit, vitae finibus ex. Nullam auctor mi nisi, in pharetra urna pharetra at. Phasellus nec arcu mauris. Etiam commodo blandit cursus. ');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `session_id` varchar(40) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `salt_token` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `uniq_info` varchar(40) NOT NULL,
  `browser` text NOT NULL,
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `updated_at`, `salt_token`, `user_id`, `uniq_info`, `browser`, `ip`) VALUES
('bKPSrNtt5cVpQkENVZDd2ykLqRI2Ng1516576920', 1516577029, 'f8nAUDRaaE', 0, 'd01ca5c8c7eaed6dac22bbd9c3ae8abf', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `User_Id` int(255) NOT NULL,
  `User_Name` char(255) NOT NULL,
  `User_Password` char(255) NOT NULL,
  `User_email` char(255) NOT NULL,
  `Admin` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`User_Id`, `User_Name`, `User_Password`, `User_email`, `Admin`) VALUES
(1, 'admin', 'admin', 'admin@admin', 1),
(2, '', 'asfd', 'dfs@df', NULL),
(3, '', 'test1', 'test1@qqq', NULL),
(4, 'test2', 'test2', 'tast@test2', NULL),
(5, 'test3', 'test3', 'test3@qqq', NULL),
(6, 'asd', 'asd', 'asdf@sadf', NULL),
(7, 'aaa', 'aaa', 'asdf@sadf', NULL),
(8, 'aaaaaa', 'as', 'aaaaaa@aa.aa', NULL),
(9, 'wer', 'wer', 'wer@rew', NULL),
(10, 'qwe', 'qwe', 'asda@asd', NULL),
(12, 'q', 'e', 'qqqqq@qq', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`Cart_Id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Category_Id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Product_Id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`User_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `Cart_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `Category_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orderproduct`
--
ALTER TABLE `orderproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `Product_Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `User_Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
