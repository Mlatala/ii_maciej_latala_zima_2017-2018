<?php
	require('header.php');
?>
<?php
		$sql = 'INSERT INTO `order`(`id`, `customer`, `address`, `email`) VALUES (null,:customer,:address,:email)';
		$result = $conn->prepare($sql);
		
		$result->bindValue(':customer',$_POST['customer'],PDO::PARAM_STR);
		if($session->getUser()->isAnonymous())
		$result->bindValue(':email',$_POST['email'],PDO::PARAM_STR);
		else
		$result->bindValue(':email',$session->getUser()->getEmail(),PDO::PARAM_STR);
		$result->bindValue(':address',$_POST['address'],PDO::PARAM_STR);
		$result->execute();
		
		$orderId = $conn->lastInsertId();
		
		$orderedProducts = $cart->getProducts();
		
		foreach($orderedProducts as $product){
		$pid = $product['pid'];
		$qty = $product['Quantity'];
			
		$sql = 'INSERT INTO orderproduct (id, order_id,product_id,quantity) VALUES (null, :orderId, :pid,:qty)';
		$result = $conn->prepare($sql);
		
		$result->bindValue('orderId',$orderId,PDO::PARAM_INT);
		$result->bindValue(':pid',$pid,PDO::PARAM_INT);
		$result->bindValue(':qty',$qty,PDO::PARAM_INT);
		$result->execute();
		}
		
		$cart->clear();
		
		?>
		<h1>Zamówienie przebiegło pomyślnie</h1>
<?php
	require('footer.php');
?>