<?php

define('SESSION_COOKIE','cookiesklep');
define('SESSION_ID_LENGHT',40);
define('SESSION_COOKIE_EXPIRE',3600);

function showMenu(){
		global $conn,$session;
			$sql = "SELECT * FROM category";
			$result = $conn->query($sql);	
	?>
<body>
<div class="container">
  <h1 class="title">E-Shop</h1>
  <ul>
    <li class="dropdown">
      <input type="checkbox" />
      <a href="#" data-toggle="dropdown">Konto</a>
      <ul class="dropdown-menu">
<?php
	  	if($session->getUser()->isAnonymous()){
?>
		<li><A HREF='login.php'>Logowanie</A></li>
		<li><A HREF='registration.php'>Rejestracja</A></li>
<?php		
		}else{
?>
		<li><A HREF='logout.php'>Wyloguj</A></li>
<?php	
		}
?>		
      </ul>
    </li>
    <li class="dropdown">
      <input type="checkbox" />
      <a href="#" data-toggle="dropdown">Produkty</a>
      <ul class="dropdown-menu">
<?php
	while($row = $result ->fetch(PDO::FETCH_ASSOC)){
				$name = $row['Category_Name'];
				$id = $row['Category_Id'];
				echo "<li><A HREF='index.php?cat_id=$id'>$name</A></li>";
				}
?>
      </ul>
    </li>
	   <li class="dropdown">
      <input type="checkbox" />
      <a href="#" data-toggle="dropdown">Produkty-Operacje</a>
      <ul class="dropdown-menu">
        <li><a href='showcart.php'>Koszyk</a></li> 

<?php		
			if($session->getUser()->isAdmin()){
?>
			<li><a href='adminAddProduct.php'>Dodaj Produkt</a></li>
		</ul>
    </li>
<?php
			}
?>			
  </ul>
</div>
</body>
<?php	/*
			
			if($session->getUser()->isAnonymous()){
			echo "<A HREF='login.php'>Logowanie</A><br>";
			echo "<A HREF='registration.php'>Rejestracja</A><br><br>";
			}else{
			echo "<A HREF='logout.php'>Wyloguj</A><br><br>";
			}
			while($row = $result ->fetch(PDO::FETCH_ASSOC)){
				$name = $row['Category_Name'];
				$id = $row['Category_Id'];
				echo "<A HREF='index.php?cat_id=$id'>$name</A>";
				echo "<br>";
			}
			echo "<br>";
			echo "<br>";
			echo "<a href='showcart.php'>Koszyk</a>";
			echo "<br>";
			if($session->getUser()->isAdmin()){
			echo "<a href='adminAddProduct.php'>Dodaj Produkt</a>";
			}
			echo "</div>"; */
		}
		
function random_session_id(){
	$utime = time();
	$id = random_salt(40-strlen($utime)).$utime;
	return $id;
}

function random_salt($length){
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>