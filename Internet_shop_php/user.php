<?php
	class user{
		private $id;
		private $login;
		private $email;
		private $admin;
		private $construct;
		
		public function __construct($anonymous = true){
			if($anonymous == true){
				$this->id=0;
				$this->email='';
				$this->login='';
				$this->admin=0;
			}			
		$this->construct =true;
		}
		
		public function setLogin($login){
			$this->login = $login;
		}
		public function setEmail($email){
			$this->email=$email;
		}
		public function getEmail(){
			return $this->email;
		}
		
		public function getLogin(){
			return $this->login;
		}
		
		public function getId(){
			return $this->id;
		}
		
		public function setId($id){
			$this->id=$id;
		}
		
		public function setAdmin($admin){
			$this->admin=$admin;
		}
		
		public function isAnonymous(){
			return ($this->id == 0);
		}
		public function isAdmin(){
			return ($this->admin == 1);
		}
		
		public function checkPassword($username,$password){
		global $conn;	
		
		$sql = 'SELECT User_Name,User_Id FROM user WHERE User_Name=:uname AND User_Password=:pword';
        $result = $conn->prepare($sql);
		
		$result->bindValue(':uname',$username,PDO::PARAM_STR);
		$result->bindValue(':pword',$password,PDO::PARAM_STR);
		$result->execute();
		
				
        if ($row = $result->fetchALL(PDO::FETCH_ASSOC)){
			$newUser = new user;
			$newUser->setId($row[0]['User_Id']);
			$newUser->setLogin($row[0]['User_Name']);
			return $newUser;
        } 
		else{
          return 0;
        }
		
		}
	}