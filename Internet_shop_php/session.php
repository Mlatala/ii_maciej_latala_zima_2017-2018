<?php

	
	class session{
		
		private $id;
		private $ip;
		private $browser;
		private $time;
		private $user;
		private $salt;
		
		
		
		public function __construct(){
			global $conn, $request;

			if(!isset($_COOKIE[SESSION_COOKIE])){
				$_COOKIE[SESSION_COOKIE] = '';
			}else{
				if(strlen($_COOKIE[SESSION_COOKIE]) != SESSION_ID_LENGHT){
					$this->newSession();
				}
			}
			
					
			$sql = 'SELECT session_id,updated_at,salt_token,user_id,uniq_info,ip,browser
			FROM session WHERE session_id=:sid AND uniq_info = :info AND updated_at > :updated
			AND ip = :ip AND browser = :browser';
			$result = $conn->prepare($sql);

			$result -> bindValue(':sid',$_COOKIE[SESSION_COOKIE], PDO::PARAM_STR);
			$result -> bindValue(':updated',time() - SESSION_COOKIE_EXPIRE, PDO::PARAM_INT);
			$result -> bindValue(':info',$request->getInfo(),PDO::PARAM_STR);
			$result -> bindValue(':ip',$request->getIp(), PDO::PARAM_STR);
			$result -> bindValue(':browser',$request->getBrowser(),PDO::PARAM_STR);
			$result -> execute();

			if($session = $result->fetch(PDO::FETCH_ASSOC)){
				$result -> closeCursor();
				$this->id = $_COOKIE[SESSION_COOKIE];
				$this->salt = $session['salt_token'];
				$this->ip = $session['ip'];
				$this->browser = $session['browser'];
				$this->time = $session['updated_at'];
					
				setcookie(SESSION_COOKIE,$this->id,time() + SESSION_COOKIE_EXPIRE);
				
				$result = $conn->prepare('UPDATE session SET updated_at = :time WHERE session_id = :sid');
				$result -> bindValue(':sid',$_COOKIE[SESSION_COOKIE], PDO::PARAM_STR);
				$result -> bindValue(':time',time(), PDO::PARAM_INT);
				$result -> execute();
				
				if($session['user_id']!=0){
					
				$result = $conn->prepare('SELECT User_Name, User_email,Admin FROM user WHERE User_Id = :uid');
				$result -> bindValue(':uid',$session['user_id'], PDO::PARAM_INT);
				$result -> execute();
				
				$row = $result->fetchAll(PDO::FETCH_ASSOC);
				$this->user = new user(true);
				$this->user->setLogin($row[0]['User_Name']);
				$this->user->setEmail($row[0]['User_email']);
				$this->user->setAdmin($row[0]['Admin']);
				$this->user->setId($session['user_id']);
				}else{
					$this->user = new user(true);
				}
			}else{
			$this->newSession();
			}
		}
		
		function newSession(){
			global $conn, $request;
			$this->id = random_session_id();
			$this->salt = random_salt(10);

			setcookie(SESSION_COOKIE, $this->id, time() +3600);
			
			$result = $conn->prepare('INSERT INTO session (session_id, updated_at,salt_token,user_id,uniq_info,browser,ip) 
			VALUES (:session_id,:time,:salt,:user_id,:info,:browser,:ip)');

			
			$result->bindValue(':session_id',$this->id, PDO::PARAM_STR);
			$result->bindValue(':time',time(), PDO::PARAM_INT);
			$result->bindValue(':salt',$this->salt, PDO::PARAM_STR);
			$result->bindValue(':user_id',0, PDO::PARAM_INT);
			$result->bindValue(':info',$request->getInfo(),PDO::PARAM_STR);
			$result->bindValue(':ip',$request->getIp(), PDO::PARAM_STR);
			$result->bindValue(':browser',$request->getBrowser(),PDO::PARAM_STR);
			$result->execute();
			$this->user=new user(true);
		}
		function updateSession (user $user){
			global $conn, $request;
			
			$newId = random_session_id();
			$newSalt = random_salt(10);
			setcookie(SESSION_COOKIE,$newId,time() + SESSION_COOKIE_EXPIRE);				
			$result = $conn->prepare('UPDATE session SET salt_token = :salt, updated_at = :time, session_id = :newId, user_id = :uid
				WHERE session_id = :sid');
									
			$result->bindValue(':time',time(), PDO::PARAM_INT);
			$result->bindValue(':salt',$newSalt, PDO::PARAM_STR);
			$result->bindValue(':uid',$user->getId(), PDO::PARAM_INT);			
			$result->bindValue(':newId',$newId, PDO::PARAM_STR);
			$result->bindValue(':sid',$this->id, PDO::PARAM_STR);	
			$result->execute();
			
			$this->id = $newId;
			$this->user = $user;
			
		}
		public function getSessionId(){
			return $this->id;
		}
		
		public function getUser(){
			return $this->user;
		}
		
	}