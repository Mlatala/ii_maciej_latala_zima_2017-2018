<?php
	require('header.php');
?>
<h1>Zawartość koszyka</h1>
<table border>
<?php
	$inCart = $cart->getProducts();
	echo "<tr><td>Indeks</td><td>Nazwa Produktu</td><td>Cena</td><td>Ilość</td><td>Wartość netto</td></tr>";
	$sum=0;
	foreach($inCart as $product){
		$productCartID = $product['Cart_Id'];
		$net_price = $product['Product_Price'];
		$quantity = $product['Quantity'];
		$index = $product['Product_Index'];
		$name = $product['Product_Name'];
		$total = $quantity * $net_price;
		$id = $product['pid'];
		$sum+= $total;
		
		$remLink = "<a href='remFromCart.php?id=$productCartID'>usuń</a>";
		$plus = "<a href='addToCart.php?id=$id'>+</a>";
		$minus="<a href='remFromCart.php?id=$id'>-</a>";
	echo "<tr><td>$index</td><td>$name</td><td>$net_price</td><td>$quantity $plus $minus</td><td>$total</td></tr>";
	}
?>
</table>
<h2>Wartość koszyka <?php echo $sum ?> zł netto</h2>

<a href='order.php'>Zamów</a>
<?php
	require('footer.php');
?>