<?php
class cart{
	
	public function __construct(){
		
	}
	
	public function add($id){
		global $conn, $session;
		
		$sql = 'SELECT * FROM cart WHERE Product_id = :id AND Session_id = :sid';
		$result = $conn->prepare($sql);
		
		$result->bindValue(':id',$id,PDO::PARAM_INT);
		$result->bindValue(':sid',$session->getSessionId(),PDO::PARAM_STR);
		$result->execute();
				
		if($row = $result->fetchAll(PDO::FETCH_ASSOC)){
		
		$qty = $row[0]['Quantity']+1;		
		
		$sql = 'UPDATE cart SET Quantity = :qty WHERE Session_id = :sid AND Product_id = :pid';
		$result = $conn->prepare($sql);

		$result->bindValue(':qty',$qty,PDO::PARAM_INT);
		$result->bindValue(':sid',$session->getSessionId(),PDO::PARAM_STR);
		$result->bindValue(':pid',$id,PDO::PARAM_INT);
		$result->execute();
		
		}		
		else{			
		$sql = 'INSERT INTO cart (Cart_Id, Session_id,Product_id, Quantity) VALUES (null, :sid, :pid, 1)';
		$result = $conn->prepare($sql);
		
		$result->bindValue(':sid',$session->getSessionId(),PDO::PARAM_STR);
		$result->bindValue(':pid',$id,PDO::PARAM_INT);
		$result->execute();
		}
	}
	public function remove($id){
		global $conn, $session;
		
		$sql = 'SELECT * FROM cart WHERE Product_Id = :id AND Session_id = :sid';
		$result = $conn->prepare($sql);
		
		$result->bindValue(':sid',$session->getSessionId(),PDO::PARAM_STR);		
		$result->bindValue(':id',$id,PDO::PARAM_INT);
		$result->execute();
		
		$row = $result->fetchAll(PDO::FETCH_ASSOC);
		$qty = $row[0]['Quantity'];
		$qty--;		
		
		if($qty==0){
			
		$sql = 'DELETE FROM cart WHERE Product_Id = :id AND Session_id = :sid';
		$result = $conn->prepare($sql);
		
		$result->bindValue(':sid',$session->getSessionId(),PDO::PARAM_STR);			
		$result->bindValue(':id',$id, PDO::PARAM_INT);
		$result->execute();
		}
		else{
			
		$sql = 'UPDATE cart SET Quantity = :qty WHERE Session_id = :sid AND Product_id = :pid';
		$result = $conn->prepare($sql);

		$result->bindValue(':qty',$qty,PDO::PARAM_INT);
		$result->bindValue(':sid',$session->getSessionId(),PDO::PARAM_STR);
		$result->bindValue(':pid',$id,PDO::PARAM_INT);
		$result->execute();
		
		}
	}
	public function getProducts(){
		global $conn,$session;
		
		$sql='SELECT s.Cart_Id, p.Product_Price, s.Quantity, p.Product_Index, p.Product_Name, p.Product_Id as pid FROM cart s LEFT OUTER JOIN product p ON (s.Product_id = p.Product_Id)WHERE Session_id = :sid';
		$result	= $conn->prepare($sql);	
		
		$result->bindValue(':sid', $session->getSessionId(),PDO::PARAM_STR);
		$result->execute();
		
		$reslt = $result->fetchAll(PDO::FETCH_ASSOC);
		return $reslt;
	}
	public function clear(){
		global $conn, $session;
		
		$sql='DELETE FROM cart WHERE Session_id = :sid';
		$result	= $conn->prepare($sql);
		
		$result->bindValue(':sid', $session->getSessionId(),PDO::PARAM_STR);
		$result->execute();
	}
}

?>